const Koa = require('koa'),
  route = require('koa-route'),
  websockify = require('koa-websocket'),
  bodyParser = require('koa-body-parser'),
  serve = require('koa-static'),
  os = require('os');

const {EventEmitter} = require('events');

/**
 * TimeSeriesCache was built to keep an ordered, max-length list of events
 * and also support basic range queries eg `getSince(time)`.
 * This isn't really put to good use in the current iteration.
 */
class TimeSeriesCache {
  constructor(maxLength = 100){
    this.map = new Map();
    this.maxLength = maxLength;
    this.last = null; // converting an iterator to an array is annoying so we just track the last element
  }

  add(timeKey, value) {
    this.map.set(timeKey, value);
    this.last = value;
    // Remove older values that exceed max length
    for (let key of this.map.keys()) {
      if(this.map.size <= this.maxLength){
        break;
      }
      this.map.delete(key)
    }

    return this.map.get(timeKey);
  }

  get(key){
    return this.map.get(key);
  }

  getSince(startTime) {
    if (!startTime) {
      // if no startTime is given, return the last event
      return this.last ? [this.last] : [];
    }
    let result = [];
    for (let eventTime of this.map.keys()){
      if (eventTime > Number(startTime)) {
        result.push(this.map.get(eventTime))
      }
    }
    return result;
  }
}

// Sets that represent a possible microservice
const daemonTokenService = new Set(['35986091353']);
const userTokenService = new Set(); // TODO: use for auth

const reportHistory = new TimeSeriesCache();
const reportEmitter = new EventEmitter();

const app = websockify(new Koa());
app.use(bodyParser());

const isProduction = process.env.NODE_ENV === 'production';

if (isProduction) {
  app.use(serve(__dirname + '/build'))
} else {
  app.use(route.get('/', function(ctx) {
    // redirect to react dev server
    ctx.response.status = 307;
    ctx.set('Location', 'http://localhost:3000/')
  }))
}

/**
 * GET /reports
 * PARAMS (opt) since: number
 * PARAMS (opt) aggregate: 'mean'
 */
app.use(route.get('/reports', function(ctx){
  // TODO: auth
  // following the TimeSeriesCache API, getSince(falsey) returns latest event.
  const {since, aggregate} = ctx.request.query;
  const safeSince = Number(since) || 0;
  let results = reportHistory.getSince(safeSince);

  if (safeSince && aggregate) {
    if (aggregate === 'mean') {
      // do you want your backend to do aggregations? Because we've got aggregations.
      let aggResult = results.reduce((acc, result) => {
        Object.keys(result).forEach(key => {
          if (key !== 'time') {
            acc[key] = (acc[key] || 0) + result[key];
          }
        })
        return acc;
      }, {})
      Object.keys(aggResult).forEach(key => {
        if (key !== 'time') {
          aggResult[key] = Math.floor(aggResult[key] / results.length);
        }
      })
      results = [aggResult];
    }
  }

  ctx.response.body = JSON.stringify(results);
}))

app.use(route.get('/user', function(ctx) {
  // TODO: we REALLY want to auth this endpoint
  const user = os.userInfo()
  ctx.response.body = JSON.stringify({name: user.username})
}))

/**
 *  POST /report
 *  BODY {time: number, ...}
 * */
app.use(route.post('/report', function(ctx){
  const token = ctx.headers['x-app-token'];
  if (!daemonTokenService.has(token)) {
    // validate that the post is from a legit daemon
    // whom we have given a valid token
    console.warn('Invalid token', token)
    ctx.response.status = 401;
    return;
  }
  // When a new report is posted, we add it to our store of 
  // recent reports, and emit an event telling our ws connection
  // to send out the new report.
  const report = ctx.request.body
  console.log('Post: report', report);
  reportEmitter.emit('report', report)
  reportHistory.add(report.time, report)
}))

/**
 * WebSocket /data-stream
 */
app.ws.use(route.all('/data-stream', function (ctx) {
  // TODO: security
  function listener(report) {
    ctx.websocket.send(JSON.stringify([report]))
  }
  reportEmitter.on('report', listener)
  ctx.websocket.on('close', () => {
    reportEmitter.removeListener('report',listener)
  })
}));

app.listen(3001);