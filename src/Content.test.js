import React from 'react';
import Content from './Content';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const el = renderer
    .create(<Content>'hello'</Content>)
    .toJSON();
  expect(el).toMatchSnapshot();
});