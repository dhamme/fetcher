import React from 'react'

function Messages(props){
  return (
    <div className='messages-wrapper'>
      <h2>Messages</h2>
      <ul className='messages-list'>
        {props.messages.map(message => {
          return (
            <li className={`messages-item ${message.type}`} key={message.id}>
              <div className='icon'>
                {
                  message.type === 'alert'
                  ? <span role='img' aria-label='fire'>🔥</span>
                  : <span role='img' aria-label='thumbs up'>👍</span>
                }
                </div>
              <span className='divider'/>
              <div className='explanation'>{message.value}</div>
            </li>
          )
        })}
      </ul>
    </div>
  )
}

export default Messages;