/* eslint-disable no-console */
/* eslint-disable no-case-declarations */
import {useEffect, useReducer, useState} from 'react';

/**
 * A simple effect for fetching and exposing a username
 */
export function useName(){
  const [name, setName] = useState('');
  useEffect(() => {
    fetch('/user')
      .then(res => res.json())
      .then(data => setName(data.name))
  }, [])
  return name;
}

/**
 * An effect that polls an endpoint and passes the response to a reducer.
 * @param {string} wsUrl Websocket address to subscribe to for event pushes
 * @param {string} fallbackHttpUrl http address to use if websockets is unsupported
 * @param {number} pollInterval frequency to poll http url in fallback situation
 */
export function useDataStream(
  wsUrl,
  fallbackHttpUrl = '',
  pollInterval = 10,
) {
  const {reducer, actionCreator, initialState} = setUpReducer('ADD', 60);
  const [dataState, dispatch] = useReducer(reducer, initialState);
  const receiveDataAction = actionCreator(dispatch);

  useEffect(() => {
    // prefer to get data via websockets, but fall back to http if unavailable
    if (WebSocket) {
      const socket = new WebSocket(wsUrl);
      socket.onmessage = event => {
        try {
          const dataArr = JSON.parse(event.data);
          receiveDataAction(dataArr[dataArr.length - 1]);
        } catch(error) {
          console.error(error);
        }
      }
      // close the connection when the component unmounts
      return () => socket.close();
    } else if (fallbackHttpUrl) {
      let lastPollTime = Date.now();
      const intervalId = setInterval(
        () => {
          const currentTime = Date.now();
          if (currentTime - lastPollTime >= pollInterval * 1000) {
            lastPollTime = currentTime;
            fetch(fallbackHttpUrl)
              .then(res => res.json())
              .then(dataArr => {
                receiveDataAction(dataArr[dataArr.length - 1])
              })
              .catch(console.error)
          }
        },
        100 // fire cb every 1/10s since we're using wall time, not cpu time  
      )
      // clear the callback when the component unmounts
      return () => clearInterval(intervalId);
    }
    
  }, [wsUrl, fallbackHttpUrl, pollInterval, receiveDataAction]);

  return dataState;
}

/**
 * A function that creates related entities (reducer, reducerState, actionCreator)
 * @param {string} actionType 
 * @param {number} historyLength max number of enties to store in events state
 */
export function setUpReducer(actionType, historyLength = 60) {
  const actionCreator = dispatch => payload => dispatch({type: actionType, payload});
  const initialState = {
    events: [], // ASC
    messages: [], // DESC
  };

  function reducer(state, action) {
    const messageLookback = 2 * 60 * 1000; // aggregate avgs over 2 minute period
    /**
     * ATTENTION: if you want a lower alert threshold, change `messageThreshold` here
     */
    const messageThreshold = 50; // we don't send floats from the server. 
    switch(action.type) {
      case actionType:
        const currentTime = new Date();
        const updatedEvents = [...state.events, action.payload].slice(-1 * historyLength);
        const avgCpu = getAvgUtilization(updatedEvents, 'cpu', currentTime, messageLookback);
        const newMessage = getMessage(state.messages, avgCpu, messageThreshold, currentTime);
        const newMessages = newMessage ? [newMessage, ...state.messages] : state.messages;
        return {
          messages: newMessages,
          events: updatedEvents,
        }
      default:
        return state;
    }
  }

  return {reducer, actionCreator, initialState};
}

/**
 * 
 * @param {[{time, [key]}]} events array ob event objects
 * @param {string} key key to access in `event` objects
 * @param {Date} currentTime time
 * @param {number} messageLookback how far in the past (in ms) do we aggregate
 */
export function getAvgUtilization(events, key, currentTime, messageLookback) {
  let sum = 0;
  let eventCount = 0;
  // iterating backwards because most recent events are pushed onto the end
  for (let i = events.length - 1; i >= 0; i--) {
    const event = events[i];
    // stop looping once we exceed the lookback window
    if (event.time < currentTime.getTime() - messageLookback) break;
    sum += event[key];
    eventCount++;
  }
  return Math.round(sum/eventCount);
}

/**
 * 
 * @param {[{type, value, id}]]} messages previous messages
 * @param {number} avgCpu recent avgCpu utilization
 * @param {number} messageThreshold amount of utilization about which we should alert
 * @param {Date} currentTime time
 */
export function getMessage(messages, avgCpu, messageThreshold, currentTime) {
  const {type: lastMessageType} = messages[0] || {};
  const id = messages.length + 1;
  let message;
  if (
    (lastMessageType === 'recovery' || !lastMessageType) 
    && avgCpu >= messageThreshold
  ){
    message = {
      id,
      type: 'alert',
      value: `High load generated an alert - cpu load = ${avgCpu}, triggered at ${currentTime.toLocaleTimeString()}`,
    }
  } else if (
    lastMessageType === 'alert' 
    && avgCpu < messageThreshold
  ){
    message = {
      id,
      type: 'recovery',
      value: `High load alert has recovered at ${currentTime.toLocaleTimeString()}`
    }
  }
  return message;
}