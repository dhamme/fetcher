import React from 'react';
import {useDataStream, useName} from './functions';
import Header from './Header';
import Graph from './Graph';
import Messages from './Messages';
import Content from './Content';
import './App.css';

function App() {
  // TODO: ensure these urls both work in prod
  const {events, messages} = useDataStream('ws://localhost:3001/data-stream', '/reports', 3);
  const name = useName();
  return (
    <div className='app'>
      <Header name={name} />
      <Content>
        <Messages messages={messages} />
        <div className='graphs-wrapper'>
          <Graph data={events} title='CPU Utilization' />
        </div>
      </Content>
    </div>
  );
}

export default App;
