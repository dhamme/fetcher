import React from 'react';

function Header(props) {
  return (
    <header>
      <div className='header content'>
        <div className='left-content'>
          <div className='logo'>
            <span role='img' aria-label='a fierce canine mascot'>🐶</span>
            </div>
          <a href='/'><h2>fetcher</h2></a>
        </div>
        <div className='right-content'>
          {props.name && `Welcome, ${props.name}!`}
        </div>
      </div>
    </header>
  )
}

export default Header;