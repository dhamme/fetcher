import React from 'react';
import LineChart from 'react-linechart';
import '../node_modules/react-linechart/dist/styles.css';

function Graph(props) {
  const processedData = props.data.map(report => ({
    ...report,
    y: report.cpu,
    x: Math.floor((report.time - Date.now()) / 1000)
  }));
  return (
    <div>
      <h2>{props.title}</h2>
      <LineChart 
        width={600}
        height={400}
        data={[{points: processedData, color: '#3023ae', id: 'cpu'}]}
        xLabel={'Time'}
        hideXLabel={true}
        xDisplay={x => x/60 <= -1 ? `${Math.floor(x/60)}m ${x%60}s` : `${x}s`}
        xMin={-10*60}
        xMax={1}
        yLabel={'% Utilization'}
        yMin={0}
        yMax={100}
        pointRadius={3}
        onPointHover={point => `${point.y}% at ${new Date(point.time).toLocaleTimeString()}`}
        tooltipClass={'tooltip'}
        ticks={8}
      />
    </div>
  )
}

export default Graph;