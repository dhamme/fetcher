import {setUpReducer, getAvgUtilization, getMessage} from './functions';

describe('getMessage', () => {
  it('creates an alert message', () => {
    const date = new Date(0);
    const message = getMessage([], 1, 0, date);
    expect(message).toStrictEqual({
      id: 1,
      type: 'alert',
      value: `High load generated an alert - cpu load = 1, triggered at ${date.toLocaleTimeString()}`,
    })
  });
  it('creates a recovery message', () => {
    const date = new Date(0);
    const message = getMessage([{type: 'alert'}], 0, 1, date);
    expect(message).toStrictEqual({
      id: 2,
      type: 'recovery',
      value: `High load alert has recovered at ${date.toLocaleTimeString()}`
    })
  })
  it('returns nothing', () => {
    const message = getMessage([]);
    expect(message).toBe(undefined);
  })
})

describe('getAvgUtilization', () => {
  const events = [
    {time: 0, x: 2},
    {time: 1, x: 4},
    {time: 2, x: 6}
  ];
  const date = new Date(3)
  it('should correctly average', () => {
    expect(getAvgUtilization(events, 'x', date, 4)).toBe(4)
  })
  it('should select the correct range', () => {
    expect(getAvgUtilization(events, 'x', date, 1)).toBe(6)
  })
})

describe('setUpReducer', () => {
  it('returns an actionCreator function', () => {
    const {actionCreator} = setUpReducer('foo', 20);
    expect(typeof actionCreator).toBe('function')
  })
  it('returns an initialState object', () => {
    const {initialState} = setUpReducer('foo', 20);
    expect(typeof initialState).toBe('object')
  })
  it('returns a reducer which can update its state', () => {
    const {reducer, initialState} = setUpReducer('foo', 20);
    const state = reducer(
      initialState,
      {type: 'foo', payload: {time: 0, cpu: 1}})
    expect(state).toStrictEqual({
      messages: [],
      events: [{time: 0, cpu: 1}]
    })
  })
})