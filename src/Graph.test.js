import React from 'react';
import Graph from './Graph';
import ReactDOM from 'react-dom';

const mockData = [
  {
    "time": 1562742484557,
    "mem": 97,
    "cpu": 39
  },
  {
    "time": 1562742494630,
    "mem": 97,
    "cpu": 45
  },
  {
    "time": 1562742504630,
    "mem": 97,
    "cpu": 45
  },
  {
    "time": 1562742514730,
    "mem": 97,
    "cpu": 48
  },
  {
    "time": 1562742524737,
    "mem": 97,
    "cpu": 52
  },
  {
    "time": 1562742534777,
    "mem": 97,
    "cpu": 57
  },
  {
    "time": 1562742544834,
    "mem": 97,
    "cpu": 54
  },
  {
    "time": 1562742554854,
    "mem": 97,
    "cpu": 55
  },
  {
    "time": 1562742564946,
    "mem": 98,
    "cpu": 63
  },
  {
    "time": 1562742575043,
    "mem": 97,
    "cpu": 61
  }
]
it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Graph data={mockData} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
