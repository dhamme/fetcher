import React from 'react';
import Header from './Header';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const el = renderer
    .create(<Header />)
    .toJSON();
  expect(el).toMatchSnapshot();
});