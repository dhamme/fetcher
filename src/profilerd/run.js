const {run} = require('./utils');

/**
 * This file simply exists to make make it easy
 * for our CLI to spawn a process that calls `run`
 * 
*/
const argv = JSON.parse(process.argv[2]);
run({
  cpu: argv.cpu,    
  destination: argv.destination,
  frequency: argv.frequency,
  logErrors: argv.debug,
  mem: argv.mem,
  token: argv.token,
})