const fetch = require('node-fetch');
const os = require('os');

/**
 * Returns object with information about the validity of destination string
 * @param {string} destination 
 */
const validateDestinationString = destination => {
  if (!destination || typeof destination !== 'string') {
    return {isValid: false, message: '\'destination\' argument must be a valid string'}
  }
  if(!destination.startsWith('http://') && !destination.startsWith('https://')) {
    return {isValid: false, message: '\'destination\' must start with \'http://\' or \'https://\''}
  }
  return {isValid: true};
}

/**
 * Function that abstracts how a payload Object gets to a destination
 * This is curried because destination doesn't change while payload does
 * @param {string} destination 
 * @param {sring} token Token used to identify & auth your daemon when making requests
 * @param {(error) => void} logger a logging function
 * @returns {(payload: Object) => Promise<*>} response
 */
const report = (destination, token = '', logger = noop) => {
  const {isValid, message} = validateDestinationString(destination)
  if (!isValid) {
    throw new Error(message)
  }

  return (payload) => {
    return new Promise((resolve) => resolve(JSON.stringify(payload)))
      .then(stringifiedPayload => fetch(
        destination, 
        {
          method: 'POST',
          body: stringifiedPayload,
          headers: {
            'Content-Type': 'application/json',
            'x-app-token': token,
          }
        }
      ))
      .catch(logger.warn)
  }
};

/**
 * Map of resource name to a function that returns that resource from the OS
 * Returns utilization as ints, not floats. eg .2334 -> 23
 */
const resourceCalls = {
  cpu: (os) => Math.min(parseInt(100 * os.loadavg()[0] / os.cpus().length, 10), 100),
  mem: (os) => Math.min(parseInt(100 * (1 - (os.freemem() / os.totalmem())), 10), 100),
}

const noop = () => {};

/**
 * 
 * @param {number} frequency duration in seconds between reports
 * @param {(payload: Object) => Promise<*>)} report function that sends a payload to a destination
 * @param {{[resourceName: string]: () => (resourceStat: number)}} resources map of resource names to functions that return that resource info
 */
function profile(frequency, report, resources = {}) {
  // frequency validation
  if (typeof frequency !== 'number' || Number(frequency) <= 0) {
    throw new Error('\'frequency\' argument must be a valid number > 0')
  }
  // report validation
  if (typeof report !== 'function') {
    throw new Error('\'report\' must be a function that accepts a destination and payload')
  }
  // resources validation
  if (Object.keys(resources).length === 0) {
    throw new Error('Please pass \'resources\' to profile. {[resourceName]: () => number}')
  }

  let lastLogTime = Date.now();
  function reportingRunner() {
    const currentTime = Date.now();

    // if not enough *real time* has passed since last report, do nothing
    if (currentTime - lastLogTime < frequency * 1000) {
      return
    }
    
    // Generate payload based on requested resources
    lastLogTime = currentTime;
    const payload = {
      time: currentTime,
      ...(
        Object.keys(resources)
        .reduce((acc, key) => ({...acc, [key]: resources[key](os)}), {})
      )
    };

    // Report payload to destination
    report(payload);
  }
  setInterval(reportingRunner, 100); // run every 1/10s
}

function run(args) {
  const {
    logErrors,
    destination,
    token,
    frequency,
    mem,
    cpu    
  } = args;
  const logger = logErrors ? console : noop; // or bring your own logger, whatever
  const reporter = report(destination, token, logger);
  profile(
    frequency,
    reporter,
    {
      ...(mem ? {mem: resourceCalls['mem']} : {}),
      ...(cpu ? {cpu: resourceCalls['cpu']} : {}),
    }
  );
}

module.exports = {
  noop,
  run,
  profile,
  report,
  resourceCalls,
  validateDestinationString,
};