/* eslint-disable no-console */
const {spawn} = require('child_process');
const ps = require('ps-node');
const yargs = require('yargs');
const {validateDestinationString} = require('./utils');

console.log(`
###########################################
###########################################
             PROFILER DAEMON
###########################################
 a lot of code to send numbers to a server
   RUN USING "--help" FLAG FOR MORE INFO
###########################################
`)

// CLI OPTIONS
const argv = yargs
  .option('debug', {
    description: 'Debug mode keeps process attached, logs errors, and pipes through all stdin/out/err',
    type: 'boolean',
  })
  .option('frequency', {
    alias: 'f',
    description: 'Poll frequency in seconds',
    default: 10,
    requiresArg: true,
    type: 'number',
  })
  .option('destination', {
    alias: 'd',
    demandOption: true,
    description: 'Address of destination to POST reports',
    type: 'string',
  })
  .option('cpu',{
    description: 'Log cpu utilization or not',
    type: 'boolean',
  })
  .option('memory', {
    alias: 'mem',
    description: 'Log memory utilization or not',
    type: 'boolean',
  })
  .option('token', {
    alias: 't',
    description: 'Token used to identify your daemon to the server it reports to',
    type: 'string',
  })
  .hide('version')
  .group(['cpu', 'memory'], 'Resources:')
  .check(argv => {
    if(!(argv.cpu || argv.memory)) {
      throw new Error('At least one of \'--cpu\' or \'--mem\' must be specified')
    }
    const {isValid: isDestinationValid, message} = validateDestinationString(argv.destination);
    if (!isDestinationValid) {
      throw new Error(message);
    }
    return true;
  })
  .help()
  .alias('help', 'h')
  .argv;

const runFilePath = './src/profilerd/run.js';
ps.lookup(
  {command: 'node', arguments: runFilePath},
  (err, results) => {
    // Try to find an existing daemon. We don't want multiple running.
    if (results.length > 0) {
      console.log('ANOTHER DAEMON IS ALREADY RUNNING,')
      console.log('NOT STARTING A NEW DAEMON.');
      console.log('KILL DAEMON PROCESS (IN OSX) USING:')
      console.log(`# kill ${results.map(({pid}) => pid).filter(pid => pid > 1).join(' ')}`)
      return;
    }

    // spawn our profiler as a child process so we can background it
    const child = spawn(
      'node',
      [runFilePath, JSON.stringify(argv)],
      {
        detached: !argv.debug,
        stdio: argv.debug ? 'pipe' : 'ignore'
      }
    );

    console.log('NEW DAEMON SPAWNED SUCCESSFULLY')
    console.log('DAEMON WILL CONTINUE RUNNING IN THE BACKGROUND')
    console.log(`KILL THIS PROCESS (IN OSX) WITH "kill ${child.pid}"`)

    if (argv.debug) {
      // Log all of the stdio data if in debug mode
      child.on('exit', (code, signal) => {
        console.log(`child process exited with code ${code} and signal ${signal}`);
      });
      child.on('error', (code, signal) => {
        console.log(`child process exited with code ${code} and signal ${signal}`);
      });
      child.stdout.on('data', (data) => console.log(`child stdout:\n${data}`));
      child.stderr.on('data', (data) => console.error(`child stderr:\n${data}`));
    } else {
      // detach the process if not in debug mode
      child.unref();
    }
  }
);