# Profiler Daemon
The Profiler Daemon creates a background process that 
polls os-level stats and sends them to a given endpoint.

This daemon comes with an intuitive and helpful CLI.
You can make use of it by simply running `node ./index.js --help`

### DESIGN NOTE: 
ideally all profilerd code would be in its own git repo
with its own tests and package.json. But its very existence 
is already far outside the scope of this coding challenge,
so we'll call it a "todo".