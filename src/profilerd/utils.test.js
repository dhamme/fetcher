import {
  noop,
  run,
  profile,
  report,
  resourceCalls,
  validateDestinationString,
} from './utils';

describe('noop', () => {
  it('does nothing', () => {
    expect(noop()).toBe(undefined);
  });
});

describe('profile', () => {
  it('throws without valid arguments', () => {
    expect(profile).toThrow();
  });
  it('repeatedly calls report function with new data', done => {
    let reportCount = 0;
    let cpuData = 100;
    const reportStub = data => {
      expect(typeof data.time).toBe('number');
      expect(data.cpu).toEqual(cpuData);
      reportCount++;
      cpuData++;
      if(reportCount > 1) {
        done();
      }
    };
    const resourcesStub = {
      cpu: (os) => cpuData,
    };
    profile(.5, reportStub, resourcesStub);
  });
});

describe('run', () => {
  it('is a function', () => {
    expect(typeof run).toBe('function');
  });
});

describe('report', () => {
  it('throws from an invalid destination', () => {
    expect(report.bind(null, 'garbage')).toThrow();
  });
  it('is a curried function', () => {
    expect(typeof report('http://localhost:3000')).toBe('function');
  });
});

describe('resourceCalls', () => {
  const osStub = {
    loadavg: () => [1],
    cpus: () => [1,2,3,4],
    freemem: () => 50,
    totalmem: () => 100,
  }
  it('mem = freemem / totalmem', () => {
    expect(resourceCalls.mem(osStub)).toBe(50);
  });
  it('cpu = loadavg[0] / cpus.length', () => {
    expect(resourceCalls.cpu(osStub)).toBe(25);
  });
});

describe('validateDestinationString', () => {
  it('fails an empty string', () => {
    expect(validateDestinationString('')).toEqual({isValid: false, message: `'destination' argument must be a valid string`});
  });
  it('fails a non-https? string', () => {
    expect(validateDestinationString('localhost:3000')).toEqual({isValid: false, message: `'destination' must start with 'http://' or 'https://'`});
  });
  it('passes a valid string', () => {
    expect(validateDestinationString('http://localhost:3000')).toEqual({isValid: true});
  });
})
