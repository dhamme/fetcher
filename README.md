# [fetcher](http://localhost:3001)

fetcher is an application for monitoring the health of your hardware.
Written by David Hamme.

## Run it
1. Install a modern version of node using [nvm](https://github.com/nvm-sh/nvm)  `nvm install 10.16.0`
2. Install project dependencies `yarn install`
3. Launch the daemon, server, and hot-relaoding react server in dev mode using `yarn start-all`. 
Alternatively you can launch each of these individually using `yarn start-daemon`, `yarn start-server`, and `yarn start-client`
4. If you'd like to do a production build and just serve those static build files from the server, run `yarn start-prod`
5. Navigate to [http://localhost:3001](http://localhost:3001) to admire your handiwork. Note that this is the server port, and you will be auto-redirected to 3000 if in dev mode.
6. Run tests using `yarn test`

## Major Architecture

This application has three major components:

### profilerd daemon

Location: `/src/profilerd`

This daemon starts itself as a background process on the host machine, collects processor/memory utilization, and sends the data to a server. It is highly configurable and allows you to configure:
- what data is collected
- how often it is collected
- where it is sent
- enter debug mode to keep process in foreground
Its CLI has detailed documentation available with `yarn start-daemon --help`

![daemon screenshot](media/profilerd.png)

### Server

Location: `/server.js`

Our backend only has a few routes:
- `/report`: an authenticated endpoint that our daemon POSTs data to.
- `/data-stream`: a websocket endpoint that pushed out new reports as they are received.
- `/reports`: endpoint that a client can short-poll for report data if WS is unsupported. It also has support for more advanced queries like "all reports since [time]" and "the average of all reports since [time]".
- `/user`: returns the os user's username.
- other: index and static routes.

### Frontend

Location: `/src`
Finally, the actual point of this whole thing, the frontend. 

Our frontend is written in React 16.8 and uses a small reducer to manage the state of `events` and `messages`. `events` are just a list of whatever comes from the server, and `messages` are derived those events as well as what previous messages exist (eg we only want to tell the user that an alert has been resolved if there has been an alert in the past)

Messages are presented in a scrollable list. Events are plotted using [react-linechart](https://www.npmjs.com/package/react-linechart).

NOTE: If you'd like to have a lower alert threshold (currently 50%), change the `messageThreshold` variable in `src/functions.js#setUpReducer`.

![frontend screenshot](media/fetcher.png)


## Reflections, Improvements, and Future Work

For starters, I should have spent 0 time on the daemon and server. But I've never written a daemon before, and that seemed to be the most appropriate pattern to use for what I was trying to accomplish, plus it was fun.

I would like to find a better graphing library that plays well with React. The solutions that I've been able to find have either been too complicated for a simple job (D3), or their default settings are overkill (plotly). Others like react-vis just gave me issues. Finding a straightforward plotting library was definitely the trickiest part of the front end. 

I'm happy with the reducer that I've written for the event/message state, however I'd like to make the app a bit more extensible and configurable. I'm already collecting memory utilization, so maybe the user can add more graphs for the data that's available. Or they will be empowered to change their alert conditions. 

I would like to add some more css polish and transitions. Specifically I'd like to transition in new messages, and maybe also transition in new points on the plot. I think offical desktop notificiations would also be a game-changing feature for an app like this.  

I think it would be fun to flesh out the backend some more and add a sign in/up flow in addition to protection for all our routes. 